<?php

namespace Empu\OneData\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            RegionSeeder::class,
            CivilDataSeeder::class,
        ]);
    }
}