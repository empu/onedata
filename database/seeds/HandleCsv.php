<?php

namespace Empu\OneData\Seeders;

/**
 * Handle csv file
 */
trait HandleCsv
{
    protected function handleCsvFile(string $path, \Closure $callback)
    {
        $handle = fopen($path, 'r');

        while (($data = fgetcsv($handle, 200, ','))) {
            $callback($data);
        }

        fclose($handle);
    }
}
