<?php

namespace Empu\OneData\Seeders;

use Empu\OneData\Models\Region;
use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    use HandleCsv;

    public function run()
    {
        $this->seedProvinces();
        $this->seedCities();
        $this->seedDistricts();
    }

    protected function seedProvinces()
    {
        $path = __DIR__.'/csv/provinces.csv';

        $this->handleCsvFile($path, function ($data) {
            $attrs = [
                'bps_code' => $data[0],
                'label' => $data[1],
                'name' => $data[1],
                'level' => Region::LEVEL_PROVINCE,
            ];

            Region::create($attrs);
        });
    }

    protected function seedCities()
    {
        $groups = [];
        $path = __DIR__.'/csv/cities.csv';

        $this->handleCsvFile($path, function ($data) use (&$groups) {
            $parentCode = $data[1];

            if (! isset($groups[$parentCode])) {
                $groups[$parentCode] = [];
            }

            $attrs = [
                'bps_code' => $data[0],
                'name' => $data[2],
                'level' => Region::LEVEL_CITY,
            ];

            array_push($groups[$parentCode], new Region($attrs));
        });

        foreach ($groups as $parentCode => $children) {
            $province = Region::byBpsCode($parentCode)->first();

            $labeledChildren = collect($children)->map(function ($city) use ($province) {
                $city->label = "{$city->name}, {$province->label}";
                return $city;
            });

            $province->children()->saveMany($labeledChildren);
        }
    }

    protected function seedDistricts()
    {
        $groups = [];
        $path = __DIR__.'/csv/districts.csv';

        $this->handleCsvFile($path, function ($data) use (&$groups) {
            $parentCode = $data[1];

            if (! isset($groups[$parentCode])) {
                $groups[$parentCode] = [];
            }

            $attrs = [
                'bps_code' => $data[0],
                'name' => $data[2],
                'level' => Region::LEVEL_DISTRICT,
            ];

            array_push($groups[$parentCode], new Region($attrs));
        });

        foreach ($groups as $parentCode => $children) {
            $city = Region::byBpsCode($parentCode)->first();

            $labeledChildren = collect($children)->map(function ($district) use ($city) {
                $district->label = "{$district->name}, {$city->label}";
                return $district;
            });

            $city->children()->saveMany($labeledChildren);
        }
    }
}