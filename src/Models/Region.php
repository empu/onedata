<?php

namespace Empu\OneData\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use InvalidArgumentException;
use Illuminate\Support\Str;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Ramsey\Uuid\Uuid;

/**
 * Region Model
 *
 * @property string $bps_code
 */
class Region extends BaseModel
{
    use SoftDeletes;

    const LEVEL_COUNTRY = 0;
    const LEVEL_PROVINCE = 1;
    const LEVEL_CITY = 2;
    const LEVEL_DISTRICT = 3;
    const LEVEL_VILLAGE = 4;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'empu_onedata_regions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['code', 'label', 'sort_order', 'is_enable'];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function generateRef(): LazyUuidFromString
    {
        return Uuid::uuid5($this->getRefNamespace(), $this->bps_code);
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(Region::class, 'parent_id');
    }

    public function children(): HasMany
    {
        return $this->hasMany(Region::class, 'parent_id');
    }

    public function getLabelAttribute($value): string
    {
        if (preg_match('~^KABUPATEN\s([\w\s]+)(,\s[.+])?$~i', $value, $matches)) {
            $value = $matches[1];
        }

        return Str::title($value);
    }

    public function scopeIsAvailable(Builder $builder, $availability = true): Builder
    {
        return $builder->where('is_available', $availability);
    }

    public function scopeByBpsCode(Builder $builder, string $code): Builder
    {
        return $builder->where('bps_code', $code);
    }

    public function scopeProvincesOnly(Builder $builder): Builder
    {
        return $this->scopeByLevel($builder, self::LEVEL_PROVINCE);
    }

    public function scopeCitiesOnly(Builder $builder): Builder
    {
        return $this->scopeByLevel($builder, self::LEVEL_CITY);
    }

    public function scopeDistrictsOnly(Builder $builder): Builder
    {
        return $this->scopeByLevel($builder, self::LEVEL_DISTRICT);
    }

    public function scopeVillagesOnly(Builder $builder): Builder
    {
        return $this->scopeByLevel($builder, self::LEVEL_VILLAGE);
    }

    public function scopeByLevel(Builder $builder, int $level): Builder
    {
        return $builder->where('level', $level);
    }

    public function scopeScope(Builder $builder, string $key): Builder
    {
        $scopes = [
            'country' => self::LEVEL_COUNTRY,
            'province' => self::LEVEL_PROVINCE,
            'city' => self::LEVEL_CITY,
            'district' => self::LEVEL_DISTRICT,
            'village' => self::LEVEL_VILLAGE,
        ];

        if (! array_key_exists($key, $scopes)) {
            throw new InvalidArgumentException('Invalid scope key.');
        }

        return $builder->byLevel($scopes[$key]);
    }
}
